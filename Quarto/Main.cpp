#include "Piece.h"
#include "Board.h"
#include "UnusedPieces.h"
#include<iostream>
#include<array>
#include<algorithm>


int main()
{
	try
	{
		Piece piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
		std::cout << piece;
		Board ourBoard;
		std::cout << ourBoard << "\n";
		ourBoard[{1, 2}] = std::move(piece);
		std::cout << ourBoard << "\n";
		UnusedPieces unusedpieces;
		std::cout << "The unused pieces are: \n" << unusedpieces;
		ourBoard[{0, 2}] = unusedpieces.PickPiece("FuDaShRo");
		std::cout << "The unused pieces are: \n" << unusedpieces;
		return 0;
	}
	catch (const char* message)
	{
		std::cout << message;
	}
}
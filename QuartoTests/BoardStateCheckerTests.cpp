#include "stdafx.h"
#include "CppUnitTest.h"

#include "BoardStateChecker.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
namespace QuartoTests
{
	TEST_CLASS(BoardStateCheckerTests)
	{
	public:
		TEST_METHOD(EmptyBoardNone)
		{
			Board board;
			auto state = BoardStateChecker::Check(board,{ 1,2 });
			Assert::IsTrue(state == BoardStateChecker::State::None);
		}
		TEST_METHOD(LineOneOneFeatureWin)
		{
			Board board;
			board[{0, 0}]=Piece(Piece::Body::Full,Piece::Color::Dark,Piece::Height::Tall,Piece::Shape::Round);
			board[{0, 1}] = Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round);
			board[{0, 2}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
			Board::Position lastPosition = {0,3};
			board[lastPosition] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
			auto state = BoardStateChecker::Check(board, { lastPosition });
			Assert::IsTrue(state == BoardStateChecker::State::Win);
		}
		TEST_METHOD(LineOneZeroFeatureWin)
		{
			Board board;
			board[{0, 0}] = Piece(Piece::Body::Hollow, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			board[{0, 1}] = Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round);
			board[{0, 2}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
			Board::Position lastPosition = { 0,3 };
			board[lastPosition] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
			auto state = BoardStateChecker::Check(board, { lastPosition });
			Assert::IsTrue(state == BoardStateChecker::State::None);
		}
		TEST_METHOD(LineThreeOneFeatureWin)
		{
			Board board;
			board[{2, 0}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			board[{2, 1}] = Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round);
			board[{2, 2}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
			Board::Position lastPosition = { 2,3 };
			board[lastPosition] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
			auto state = BoardStateChecker::Check(board, { lastPosition });
			Assert::IsTrue(state == BoardStateChecker::State::Win);
		}
		TEST_METHOD(ColumnOneOneFeatureWin)
		{
			Board board;
			board[{0, 0}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			board[{1, 0}] = Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round);
			board[{2, 0}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
			Board::Position lastPosition = { 3,0 };
			board[lastPosition] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
			auto state = BoardStateChecker::Check(board, { lastPosition });
			Assert::IsTrue(state == BoardStateChecker::State::Win);
		}
		TEST_METHOD(ColumnFourOneFeatureWin)
		{
			Board board;
			board[{0, 3}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			board[{1, 3}] = Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round);
			board[{2, 3}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
			Board::Position lastPosition = { 3,1 };
			board[lastPosition] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
			auto state = BoardStateChecker::Check(board, { lastPosition });
			Assert::IsTrue(state == BoardStateChecker::State::Win);
		}
		TEST_METHOD(SecondDiagonalOneFeatureWin)
		{
			Board board;
			board[{0, 0}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			board[{0, 1}] = Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round);
			board[{0, 2}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
			Board::Position lastPosition = { 0,3 };
			board[lastPosition] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
			auto state = BoardStateChecker::Check(board, { lastPosition });
			Assert::IsTrue(state == BoardStateChecker::State::Win);
		}
		TEST_METHOD(MainDiagonalOneFeatureWin)
		{
			Board board;
			board[{0, 3}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			board[{1, 2}] = Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round);
			board[{2, 1}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
			Board::Position lastPosition = { 3,3 };
			board[lastPosition] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
			auto state = BoardStateChecker::Check(board, { lastPosition });
			Assert::IsTrue(state == BoardStateChecker::State::Win);
		}
	
		TEST_METHOD(SecondDiagonalTwoFeatureWin)
		{
			Board board;
			board[{0, 3}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			board[{1, 2}] = Piece(Piece::Body::Full, Piece::Color::Light, Piece::Height::Tall, Piece::Shape::Round);
			board[{2, 1}] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Short, Piece::Shape::Round);
			//TO DO: fill all board
			Board::Position lastPosition = { 3,3 };
			board[lastPosition] = Piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Square);
			auto state = BoardStateChecker::Check(board, { lastPosition });
			Assert::IsTrue(state == BoardStateChecker::State::Win);
		}
	};
}
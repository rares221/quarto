#include "stdafx.h"
#include "CppUnitTest.h"

#include "Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuartoTests
{
	TEST_CLASS(BoardTests)
	{
	public:
		TEST_METHOD(LineOneColumnMinusOne)
		{
			Board board;
			board[{1, -1}];
			Assert::ExpectException<const char>([&board]() {board[{1, -1}];
				});
		}
		TEST_METHOD(LineOneColumnMinusOneConst)
		{
			const Board board;
			board[{1, -1}];
			Assert::ExpectException<std::out_of_range>([&board]() {board[{1, -1}];
				});
		}
	

	};
	
	
}